import Foundation

struct Song{
    var songName : String
    var album : Album?  // album is optional
    var language : Languages
    var genre : Genre
    var songWriter : Artist
    var length : Int
    var url : URL
    init(songName: String,language:Languages,genre:Genre,songWriter:Artist,length:Int,url:URL){
        self.songName = songName
        self.language = language
        self.genre = genre
        self.songWriter = songWriter
        self.length = length
        self.url = url
    }
    init(songName:String,album:Album,genre:Genre,songWriter:Artist,length:Int,url:URL){
        self.songName = songName
        self.genre = genre
        self.length = length
        self.songWriter = songWriter
        self.language = album.language
        self.album = album
        self.url = url
    }
}

struct Album{
    var albumName : String
    var songs : [Song]
    var director : Artist
    var artist : Artist
    var producers : [Artist]
    var releaseDate : Int
    var releaseMonth : Int
    var releaseYear : Int
    var country : String
    var language : Languages
    
    
    init(albumName:String,songs:[Song],director:Artist,musicBy:Artist,producers:[Artist],releaseDate:Int,releaseMonth:Int,releaseYear:Int,country:String,language:Languages){
        self.albumName = albumName
        self.songs = songs
        self.director = director
        self.artist = musicBy
        self.producers = producers
        self.releaseDate = releaseDate
        self.releaseYear = releaseYear
        self.releaseMonth = releaseMonth
        self.country = country
        self.language = language
    }
}

struct Artist{
    var artistName : String
    var age : Int
    var albums : [Album]? = []
    var image : Image? //optional image
    var url : URL
    
}

struct Image{
    var height : Int
    var width : Int
    var url : URL
    
}
