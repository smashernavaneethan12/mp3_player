enum Languages{
    case tamil
    case english
    case telugu
    case hindi
    case malayalam
}

enum Genre{
    case melody
    case rock
    case classical
    case western
}

